# OpenFlexure Microscope Technical Overview

This repository indexes and collates the various pieces of technical documentation relating to the OpenFlexure Microscope.  It is intended as a starting point for manufacturers setting out to develop a product based on the OpenFlexure Microscope repositories.  It collects together information about the design, the processes we have followed in creating and maintaining the design, and how the OpenFlexure Microscope has been assessed.  A manufacturer will need to update, extend, and modify it to account for their own production processes, market, and intended use.  Any entity selling products based on the OpenFlexure Microscope design takes full responsibility for those products, including any regulatory requirements, as set out in the provisions of our various open source licenses and our [medical devices FAQ page].

> **Please Note:** This technical overview is very much under construction, and is at present an incomplete summary of the various documents that exist elsewhere in the OpenFlexure repositories on GitLab, and elsewhere.

## Table of Contents

### Description and assessment

* **TODO**: Specifications
* General description
    * [One-page description](description.md) of the microscope
    * A good overview is given in the [Biomedical Optics Express paper][Collins2020]
    * **TODO**: Block diagram
    * **TODO**: Wiring/circuit diagrams
        * [Sangaboard](https://kitspace.org/boards/gitlab.com/bath_open_instrumentation_group/sangaboard/sangaboard_v0.4/)
    * **TODO**: Drawings
    * Control philosophy/logic
        * [Software stack paper][Collins2021] covers the software architecture and technologies employed
        * [Overview paper][Collins2020]
        * [Autofocus paper][Knapper2021]
        * **TODO**: Flowcharts or descriptions of important procedures e.g. slide scanning, autofocus
    * **TODO**: Datasheets for critical sub-assemblies
        * **TODO**: Pi
        * **TODO**: Sangaboard?
        * **TODO**: Objective??
* **TODO**: List of standards applied
    * **TODO**: Records of assessments to standard
* Risk
    * [Failure Mode and Effects Analysis (FMEA)](https://gitlab.com/openflexure/microscope-failure-mode-analysis) (**TODO**: needs significant work)
    * [Risk-related meeting minutes](https://gitlab.com/openflexure/microscope-failure-mode-analysis/-/tree/master/meetings/)
    * **TODO**: extract documents from our risk register that better match the format of documents expected by regulators/auditors.
    * **TODO**: Add required **records** relating to risk
* Verification and validation
    * Analytical/Technical performance
        * [Closed loop autofocus][Knapper2021]
        * [Mechanical stability][Sharkey2016]
        * **TODO**: Resolution paper
    * **TODO**: Clinical evidence
    * **TODO**: Scientific validity
    * **TODO**: Performance evaluation plan - necessarily incomplete, this must be done by manufacturer
* **TODO**: GSPR and compliance with Annex 1 of IVDR (applicable in Tz?)

### Instructions

* [Assembly instructions](https://build.openflexure.org/openflexure-microscope/v7.0.0-alpha2/)
    * [Parts list](https://build.openflexure.org/openflexure-microscope/v7.0.0-alpha2/high_res_microscope_BOM.html)
    * [Software installation](https://openflexure.org/projects/microscope/install)
    * **TODO**: Go all the way to being ready to image and/or calibrate
* Usage instructions
	* [Software getting-started](https://openflexure.org/projects/microscope/control)
    * [Software usage](https://openflexure-microscope-software.readthedocs.io/en/master/webapp/index.html)
	* **TODO**: first-run instructions (which should also be in assembly instructions)
	* **TODO**: Basic fault-finding and operation
	* **TODO**: Maintenance instructions
	* **TODO**: Installation instructions
* **TODO**: Calibration/commissioning procedures
    * **TODO**: Document the automated procedures
        *(Richard has a script for the IHI microscopes, does mechanical calibration OK)*
    * **TODO**: Document the manual checks
    * **TODO**: Implement and document optical resolution self-tests
* **TODO**: Quality control
    * **TODO**: Visual guide to check prints and assembly (need to update [our old document](https://www.flickr.com/photos/192884270@N08/51128723265/sizes/l/))
    * **TODO**: Checking output of calibration/commissioning scripts
    * **TODO**: This section is very much not complete

### Design files and source code

* Hardware designs
    * [Microscope hardware and assembly instructions (source)](https://gitlab.com/openflexure/openflexure-microscope/)
    * **TODO**: pull in specific versions of STLs
    * [Sangaboard (motor controller)](https://gitlab.com/bath_open_instrumentation_group/sangaboard)
    * [Illumination board](https://gitlab.com/openflexure/openflexure-constant-current-illumination)
* Software source code
    * [Microscope server](https://gitlab.com/openflexure/openflexure-microscope-server/) (This code runs on the built-in Raspberry Pi, and includes the web application for the user interface.)
    * [Microscope operating system](https://gitlab.com/openflexure/pi-gen) (This collates all software and builds a pre-written SD card image for the Raspberry Pi.)
    * [OpenFlexure Connect](https://gitlab.com/openflexure/openflexure-connect) (Electron application to discover microscopes and display the web application.)

## Quality manual for microscope development??
* What's the minimum set of procedures to document?
	* How do we control changes to the design? -> MR guidelines
* How do we decide what changes to make? Do some light touch documentation of Spec -> design -> block diagram

[Collins2020]: https://doi.org/10.1364/BOE.385729
[Collins2021]: https://doi.org/10.1098/rsos.211158
[Knapper2021]: https://doi.org/10.1111/jmi.13064
[Sharkey2016]: https://doi.org/10.1063/1.4941068
[medical devices FAQ page]: https://openflexure.org/about/medical-devices